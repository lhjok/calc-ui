use std::sync::Arc;
use calc::Calc;
use druid::widget::{
    CrossAxisAlignment, Flex, Label, Painter,
    List, Scroll, LineBreaking, Axis
};

use druid::{
    theme, AppLauncher, Color, Data, FontDescriptor, LifeCycleCtx,
    LayoutCtx, PaintCtx,FontFamily, FontWeight,Lens, LocalizedString,
    KeyOrValue, LifeCycle, Env,BoxConstraints,RenderContext, Widget,
    WidgetExt, Event, WindowDesc, EventCtx, UpdateCtx, Size
};

#[derive(Clone, Data, PartialEq)]
enum State {
    Set,
    None
}

pub enum ScrollPos {
    None,
    Bottom
}

#[derive(Clone, Data, Lens)]
struct CalcState {
    show: String,
    value: String,
    history: Arc<Vec<CalcResult>>,
    state: State
}

#[derive(Clone, Data)]
struct CalcResult {
    result: Option<(String, String)>
}

pub struct CustomScroll<T, W> {
    child: Scroll<T, W>,
    with_scroll_pos: Box<dyn Fn(&T) -> ScrollPos>,
}

impl<T: Data, W: Widget<T>> CustomScroll<T, W> {
    pub fn new(widget: W, with_scroll_pos: impl Fn(&T) -> ScrollPos + 'static) -> Self {
        CustomScroll {
            child: Scroll::new(widget).vertical().disable_scrollbars(),
            with_scroll_pos: Box::new(with_scroll_pos),
        }
    }
}

impl<T: Data, W: Widget<T>> Widget<T> for CustomScroll<T, W> {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        self.child.event(ctx, event, data, env);
    }

    fn lifecycle(&mut self, ctx: &mut LifeCycleCtx, event: &LifeCycle, data: &T, env: &Env) {
        self.child.lifecycle(ctx, event, data, env);
    }

    fn update(&mut self, ctx: &mut UpdateCtx, old_data: &T, data: &T, env: &Env) {
        self.child.update(ctx, old_data, data, env);
    }

    fn layout(&mut self, ctx: &mut LayoutCtx, bc: &BoxConstraints, data: &T, env: &Env) -> Size {
        self.child.layout(ctx, bc, data, env);
        let scroll = (self.with_scroll_pos)(data);
        if let ScrollPos::Bottom = scroll {
            let pos = self.child.child_size().height;
            if pos > 165.0 {
                self.child.scroll_to_on_axis(ctx, Axis::Vertical, pos);
            }
        }
        self.child.layout(ctx, bc, data, env)
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &T, env: &Env) {
        self.child.paint(ctx, data, env);
    }
}

impl CalcResult {
    fn express(&self) -> String {
        self.result.as_ref().map(|value| value.0.to_string())
            .unwrap_or_default()
    }

    fn result(&self) -> String {
        self.result.as_ref().map(|value| value.1.to_string())
            .unwrap_or_default()
    }
}

fn trunc(len: String) -> String {
    let valid = len.chars().into_iter()
        .map(|x| x.to_string()).collect::<Vec<_>>();

    if valid.len() > 37 {
        valid[valid.len()-37..].concat()
    } else {
        valid.concat()
    }
}

fn oper_repl(repl: String) -> String {
    repl.replace("÷", "/")
        .replace("×", "*")
        .replace("π", "P")
        .replace("γ", "E")
}

fn func_digit_event(label: String, data: &mut CalcState) {
    if let State::Set = data.state {
        data.value = label.clone();
        data.show = label.clone();
        data.state = State::None;
    } else if data.value == "0" {
        data.value = label.clone();
        data.show = label.clone();
    } else {
        data.value += &label;
        data.show = trunc(data.value.clone());
    }
}

fn oper_event(op: &str, data: &mut CalcState, label: String) -> bool {
    match op {
        "C" => {
            data.value = String::from("0");
            data.show = String::from("0");
        },
        "🡨" => {
            if data.value.len() == 1 {
                data.value = String::from("0");
                data.show = String::from("0");
            } else {
                data.value.pop();
                data.show = trunc(data.value.clone());
            }
        },
        "=" => {
            data.state = State::Set;
            let expr = oper_repl(data.value.clone());
            if data.value != "0" {
                match Calc::new(expr).run_round(Some(7)) {
                    Ok(valid) => {
                        data.value = valid.clone();
                        data.show = trunc(valid)
                    },
                    Err(msg) => {
                        data.value = String::from("0");
                        data.show = msg
                    }
                }
                return true;
            }
        },
        "." => {
            if let State::Set = data.state {
                data.value = String::from("0");
                data.show = String::from("0");
                data.state = State::None;
            } else {
                data.value += &label;
                data.show = trunc(data.value.clone());
            }
        },
        ch @ "(" | ch @ "−" | ch @ "π" | ch @ "γ" => {
            if let State::Set = data.state {
                data.state = State::None;
                if ch == "−" && data.value != "0" {
                    data.value += &label;
                    data.show = trunc(data.value.clone());
                } else {
                    data.value = label.clone();
                    data.show = label.clone();
                }
            } else if data.value == "0" {
                data.value = label.clone();
                data.show = label.clone();
            } else {
                data.value += &label;
                data.show = trunc(data.value.clone());
            }
        },
        _ => {
            if let State::Set = data.state {
                data.value += &label;
                data.show = trunc(data.value.clone());
                data.state = State::None;
            } else {
                data.value += &label;
                data.show = trunc(data.value.clone());
            }
        },
    }
    false
}

fn func_label(fun: &str, label: String) -> impl Widget<CalcState> {
    let painter = Painter::new(|ctx, _, _env| {
        let bounds = ctx.size().to_rect();
        ctx.fill(bounds, &Color::rgb8(0x50, 0x85, 0x0));

        if ctx.is_hot() {
            ctx.stroke(bounds.inset(-0.5), &Color::WHITE, 1.0);
        }
        if ctx.is_active() {
            ctx.fill(bounds, &Color::rgb8(0x60, 0x95, 0x10));
        }
    });

    Label::new(fun.to_string())
        .with_font(FontDescriptor::with_weight(
            FontDescriptor::new(FontFamily::new_unchecked("Consolas")),
            FontWeight::BOLD))
        .with_text_size(17.)
        .center()
        .background(painter)
        .expand()
        .on_click(move |_ctx, data: &mut CalcState, _env| {
            func_digit_event(label.clone(), data)
        })
}

fn oper_label(op: String, label: String, size: impl Into<KeyOrValue<f64>>) -> impl Widget<CalcState> {
    let painter = Painter::new(|ctx, _, env| {
        let bounds = ctx.size().to_rect();
        ctx.fill(bounds, &env.get(theme::PRIMARY_DARK));

        if ctx.is_hot() {
            ctx.stroke(bounds.inset(-0.5), &Color::WHITE, 1.0);
        }
        if ctx.is_active() {
            ctx.fill(bounds, &env.get(theme::PRIMARY_LIGHT));
        }
    });

    Label::new(op.clone())
        .with_font(FontDescriptor::with_weight(
            FontDescriptor::new(FontFamily::new_unchecked("Consolas")),
            FontWeight::BOLD))
        .with_text_size(size)
        .center()
        .background(painter)
        .expand()
        .on_click(move |_ctx, data: &mut CalcState, _env| {
            let expr = data.value.clone();
            if oper_event(&op, data, label.clone()) {
                let valid = if data.show[0..1].parse::<f64>().is_ok() {
                    data.value.clone()
                } else { data.show.clone() };
                let to_list = CalcResult {
                    result: Some((expr, valid))
                };
                Arc::make_mut(&mut data.history).push(to_list);
            }
        })
}

fn operator(op: String, size: impl Into<KeyOrValue<f64>>) -> impl Widget<CalcState> {
    oper_label(op.clone(), op, size)
}

fn digit(digit: String) -> impl Widget<CalcState> {
    let painter = Painter::new(|ctx, _, env| {
        let bounds = ctx.size().to_rect();
        ctx.fill(bounds, &env.get(theme::BACKGROUND_LIGHT));

        if ctx.is_hot() {
            ctx.stroke(bounds.inset(-0.5), &Color::WHITE, 1.0);
        }
        if ctx.is_active() {
            ctx.fill(bounds, &Color::rgb8(0x71, 0x71, 0x71));
        }
    });

    Label::new(digit.clone())
        .with_font(FontDescriptor::with_weight(
            FontDescriptor::new(FontFamily::new_unchecked("Consolas")),
            FontWeight::BOLD))
        .with_text_size(24.)
        .center()
        .background(painter)
        .expand()
        .on_click(move |_ctx, data: &mut CalcState, _env| {
            func_digit_event(digit.clone(), data)
        })
}

fn history_list() -> impl Widget<CalcState> {
    let list = CustomScroll::new(
        List::new(make_list_item)
            .lens(CalcState::history)
            .fix_width(575.0),
        |data| {
            if !data.history.is_empty() {
                ScrollPos::Bottom
            } else {
                ScrollPos::None
            }
        });

    Flex::column()
        .cross_axis_alignment(CrossAxisAlignment::Start)
        .with_flex_child(list, 1.0)
        .fix_size(575.0, 165.0)
        .padding(11.0)
        .background(theme::BACKGROUND_LIGHT)
}

fn make_list_item() -> impl Widget<CalcResult> {
    Flex::column()
        .with_spacer(5.0)
        .with_child(
            Label::dynamic(|d: &CalcResult, _| format!("{}=", d.express()))
                .with_line_break_mode(LineBreaking::WordWrap)
                .with_font(FontDescriptor::with_weight(
                    FontDescriptor::new(FontFamily::new_unchecked("Consolas")),
                    FontWeight::MEDIUM))
                .with_text_size(19.0)
                .fix_width(553.0)
        )
        .with_spacer(2.0)
        .with_child(
            Label::dynamic(|d: &CalcResult, _| d.result())
                .with_text_color(theme::PRIMARY_DARK)
                .with_line_break_mode(LineBreaking::WordWrap)
                .with_font(FontDescriptor::with_weight(
                    FontDescriptor::new(FontFamily::new_unchecked("Consolas")),
                    FontWeight::BOLD))
                .with_text_size(19.0)
                .padding(3.0)
                .border(Color::rgb8(0x29, 0x29, 0x29), 1.0)
                .rounded(3.0)
                .fix_width(553.0)
                .background(theme::BACKGROUND_DARK)
        )
}

fn letter_spelling() -> impl Widget<CalcState> {
    Flex::column()
        .with_flex_child(
            Flex::row()
                .with_flex_child(func_label("A", String::from("a")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("B", String::from("b")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("C", String::from("c")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("D", String::from("d")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("E", String::from("e")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("F", String::from("f")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("G", String::from("g")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("H", String::from("h")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("I", String::from("i")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("L", String::from("l")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("N", String::from("n")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("O", String::from("o")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("P", String::from("p")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("Q", String::from("q")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("R", String::from("r")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("S", String::from("s")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("T", String::from("t")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("X", String::from("x")), 1.0), 1.0)
        .fix_size(575.0, 31.0)
}

fn build_calc() -> impl Widget<CalcState> {
    let display = Label::new(|data: &String, _env: &_| data.clone())
        .with_font(FontDescriptor::with_weight(
            FontDescriptor::new(FontFamily::new_unchecked("Consolas")),
            FontWeight::BOLD))
        .with_text_size(28.0)
        .lens(CalcState::show)
        .padding(8.0);

    Flex::column()
        .with_child(history_list())
        .with_spacer(10.0)
        .with_child(display)
        .with_spacer(5.0)
        .with_child(letter_spelling())
        .cross_axis_alignment(CrossAxisAlignment::End)
        .with_spacer(1.0)
        .with_flex_child(
            Flex::row()
                .with_flex_child(digit(String::from("7")), 1.0).with_spacer(1.0)
                .with_flex_child(digit(String::from("8")), 1.0).with_spacer(1.0)
                .with_flex_child(digit(String::from("9")), 1.0).with_spacer(1.0)
                .with_flex_child(operator(String::from("÷"), 25.), 1.0).with_spacer(1.0)
                .with_flex_child(operator(String::from("🡨"), 18.), 1.0).with_spacer(1.0)
                .with_flex_child(operator(String::from("C"), 24.), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("Cos", String::from("cos(")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("Sin", String::from("sin(")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("Tan", String::from("tan(")), 1.0), 1.0)
        .with_spacer(1.0)
        .with_flex_child(
            Flex::row()
                .with_flex_child(digit(String::from("4")), 1.0).with_spacer(1.0)
                .with_flex_child(digit(String::from("5")), 1.0).with_spacer(1.0)
                .with_flex_child(digit(String::from("6")), 1.0).with_spacer(1.0)
                .with_flex_child(operator(String::from("×"), 25.), 1.0).with_spacer(1.0)
                .with_flex_child(operator(String::from("("), 24.), 1.0).with_spacer(1.0)
                .with_flex_child(operator(String::from(")"), 24.), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("Cosh", String::from("cosh(")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("Sinh", String::from("sinh(")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("Tanh", String::from("tanh(")), 1.0), 1.0)
        .with_spacer(1.0)
        .with_flex_child(
            Flex::row()
                .with_flex_child(digit(String::from("1")), 1.0).with_spacer(1.0)
                .with_flex_child(digit(String::from("2")), 1.0).with_spacer(1.0)
                .with_flex_child(digit(String::from("3")), 1.0).with_spacer(1.0)
                .with_flex_child(oper_label(String::from("−"), String::from("-"), 25.), 1.0).with_spacer(1.0)
                .with_flex_child(operator(String::from("π"), 24.), 1.0).with_spacer(1.0)
                .with_flex_child(oper_label(String::from("ᐱ"), String::from("^"), 17.), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("Abs", String::from("abs(")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("Log", String::from("logx(")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("Sqrt", String::from("sqrt(")), 1.0), 1.0)
        .with_spacer(1.0)
        .with_flex_child(
            Flex::row()
                .with_flex_child(operator(String::from("%"), 24.), 1.0).with_spacer(1.0)
                .with_flex_child(digit(String::from("0")), 1.0).with_spacer(1.0)
                .with_flex_child(operator(String::from("."), 24.), 1.0).with_spacer(1.0)
                .with_flex_child(operator(String::from("+"), 25.), 1.0).with_spacer(1.0)
                .with_flex_child(operator(String::from("γ"), 22.), 1.0).with_spacer(1.0)
                .with_flex_child(operator(String::from("="), 24.), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("Fac", String::from("fac(")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("Ln", String::from("ln(")), 1.0).with_spacer(1.0)
                .with_flex_child(func_label("Exp", String::from("exp(")), 1.0), 1.0)
}

pub fn main() {
    let window = WindowDesc::new(build_calc())
        .window_size((575.0, 494.0))
        .resizable(false)
        .title(
            LocalizedString::new("calc-window-title")
                .with_placeholder("Senior Calculator")
        );
    let calc_state = CalcState {
        show: String::from("0"),
        value: String::from("0"),
        history: Arc::new(Vec::new()),
        state: State::None
    };
    AppLauncher::with_window(window)
        .launch(calc_state)
        .expect("launch failed");
}
